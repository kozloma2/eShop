package cz.cvut.eshop.storage;

import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.Order;
import cz.cvut.eshop.shop.ShoppingCart;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;

public class StorageTest {

    @Test
    public void getStockEntries_noEntries_shouldReturnEmptyCollection() {
        Storage storage = new Storage();
        assertTrue(storage.getStockEntries().isEmpty());
    }

    @Test
    public void getStockEntries_insertNewEntry_shouldReturnListWithOneEntry() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);
        storage.insertItems(item, 1);

        assertEquals(1, storage.getStockEntries().size());

        Iterator<ItemStock> it = storage.getStockEntries().iterator();
        assertEquals(item.getID(), it.next().getItem().getID());
        assertFalse(it.hasNext());
    }

    @Test
    public void getStockEntries_insertSameEntry_shouldIncreaseCount() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);
        storage.insertItems(item, 1);
        storage.insertItems(item, 1);

        assertEquals(1, storage.getStockEntries().size());

        Iterator<ItemStock> it = storage.getStockEntries().iterator();
        assertEquals(2, it.next().getCount());
    }

    @Test(expected = NoItemInStorage.class)
    public void removeItems_noItems_shouldThrowException() throws NoItemInStorage {
        Storage storage = new Storage();

        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);

        storage.removeItems(item, 10);
    }

    @Test(expected = NoItemInStorage.class)
    public void removeItems_insertOneItem_removeTwoItems_shouldThrowException() throws NoItemInStorage {
        Storage storage = new Storage();

        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);

        storage.insertItems(item, 1);

        storage.removeItems(item, 2);
    }

    @Test
    public void removeItems_insertItem_removeItem_shouldDecreaseCount() throws NoItemInStorage {
        Storage storage = new Storage();

        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);

        storage.insertItems(item, 1);
        storage.removeItems(item, 1);

        assertEquals(0, storage.getStockEntries().iterator().next().getCount());
    }

    @Test
    public void processOrder_withActualData() throws NoItemInStorage {
        Order order = new Order(new ShoppingCart());

        ArrayList<Item> items = new ArrayList<>();
        Item item1 = new StandardItem(123, "item1", 12, "category", 12);
        Item item2 = new StandardItem(124, "item1", 12, "category", 12);
        items.add(item1);
        items.add(item2);
        order.setItems(items);

        Storage storage = new Storage();
        storage.insertItems(item1, 5);
        storage.insertItems(item2, 5);

        storage.processOrder(order);
    }

    @Test
    public void processOrder_emptyShoppingCard_shouldJustRun() throws NoItemInStorage {
        Order order = new Order(new ShoppingCart());
        new Storage(new HashMap<>()).processOrder(order);
    }

    @Test(expected = NoItemInStorage.class)
    public void processOrder_itemNotInStorage_shouldThrowException() throws NoItemInStorage {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "apple", 1f, "food", 15));
        Order order = new Order(cart);
        new Storage().processOrder(order);
    }

    @Test
    public void getItemCount_noSuchItem_shouldReturnZero() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);
        storage.insertItems(item, 1);

        StandardItem unknownItem = new StandardItem(10, "apple", 1f, "food", 15);

        assertEquals(0, storage.getItemCount(unknownItem));
    }

    @Test
    public void getItemCount_itemFound_shouldReturnNumberOfInsertedItems() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);
        storage.insertItems(item, 15);

        assertEquals(15, storage.getItemCount(item));
    }

    @Test
    public void getItemCount_byId_noSuchItem_shouldReturnZero() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);
        storage.insertItems(item, 1);

        StandardItem unknownItem = new StandardItem(10, "apple", 1f, "food", 15);

        assertEquals(0, storage.getItemCount(unknownItem.getID()));
    }

    @Test
    public void getItemCount_byId_itemFound_shouldReturnNumberOfInsertedItems() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);
        storage.insertItems(item, 15);

        assertEquals(15, storage.getItemCount(item.getID()));
    }

    @Test
    public void getPriceOfWholeStock_emptyStack_shouldReturnZero() {
        Storage storage = new Storage();

        assertEquals(0, storage.getPriceOfWholeStock());
    }

    @Test
    public void getPriceOfWholeStock_nonEmptyStack_shouldReturnSumOfPrices() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 55F, "food", 15);

        storage.insertItems(item, 1);

        assertEquals(55F, storage.getPriceOfWholeStock(), 0);
    }

    @Test
    public void getItemsOfCategorySortedByPrice_unknownCategory_shouldReturnEmptyColection() {
        Storage storage = new Storage();
        assertTrue(storage.getItemsOfCategorySortedByPrice("unknown").isEmpty());

    }

    @Test(expected = RuntimeException.class)
    public void sortItemsByPrice_throwsExceptionOnNull() {
        ArrayList<Item> items = null;
        Storage s = new Storage();
        s.sortItemsByPrice(items);
    }

    @Test
    public void sortItemsByPrice_sortsTwoItems() {
        ArrayList<Item> items = new ArrayList<>();
        Item item = new StandardItem(123, "name", 10, "Category", 123);
        Item item2 = new StandardItem(123, "name", 1, "Category", 123);
        Item item1 = new StandardItem(123, "name", 2, "Category", 123);
        items.add(item);
        items.add(item2);
        items.add(item1);

        new Storage().sortItemsByPrice(items);
    }


    @Test
    public void printListOfItems_runs() {
        ArrayList<Item> items = new ArrayList<>();
        Storage s = new Storage();
        Item item = new StandardItem(123, "name", 10, "Category", 123);
        s.insertItems(item, 1);
        s.printListOfStoredItems();
    }

    @Test
    public void getByCategory_withActuallData() {
        Storage s = new Storage();

        ArrayList<Item> items = new ArrayList<>();
        Item item = new StandardItem(123, "name", 10, "Category", 123);
        s.insertItems(item, 1);

        List<Item> resultItems = s.getItemsByCategory("Category");
        assertTrue(resultItems.get(0).equals(item));
    }

}