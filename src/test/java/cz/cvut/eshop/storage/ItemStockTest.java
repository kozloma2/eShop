package cz.cvut.eshop.storage;

import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ItemStockTest {

    @Test
    public void increaseItemCount() {
        ItemStock itemStock = new ItemStock(null);
        itemStock.IncreaseItemCount(10);

        assertEquals(10, itemStock.getCount());
    }

    @Test
    public void decreaseItemCount() {
        ItemStock itemStock = new ItemStock(null);
        itemStock.IncreaseItemCount(10);
        itemStock.decreaseItemCount(10);

        assertEquals(0, itemStock.getCount());
    }

    @Test(expected = RuntimeException.class)
    public void decreaseItemCount_lowerThanZeno() {
        ItemStock itemStock = new ItemStock(null);
        itemStock.decreaseItemCount(10);
    }

    @Test(expected = RuntimeException.class)
    public void toString_throwsExceptionOnNullItem() {
        ItemStock itemStock = new ItemStock(null);
        itemStock.toString();
    }

    @Test
    public void countIsZeroOnInit() {
        ItemStock itemStock = new ItemStock(new StandardItem(123, "name", 123, "Category", 123));
        assertTrue(itemStock.getCount() == 0);
    }
}