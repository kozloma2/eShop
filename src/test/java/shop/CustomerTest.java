package shop;

import cz.cvut.eshop.shop.Customer;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CustomerTest {

    Customer customer;
    @Before
    public void setUp() throws Exception {
        customer = new Customer(500);
    }

    @Test
    public void getLoyaltyPoints() {
        assertEquals(500,customer.getLoyaltyPoints());
    }

    @Test
    public void getCustomerName() {
        assertEquals(null,customer.getCustomerName());
    }

    @Test
    public void setCustomerAddress() {
        customer.setCustomerName("Vaclavak");
        assertEquals("Vaclavak",customer.getCustomerName());
    }

    @Test
    public void setCustomerName() {
        customer.setCustomerName("Pepa");
        assertEquals("Pepa",customer.getCustomerName());
    }

    @Test
    public void getCustomerAddress() {
        assertEquals(null,customer.getCustomerAddress());
    }
}